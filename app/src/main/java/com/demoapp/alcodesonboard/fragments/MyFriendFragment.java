package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.viewmodels.MyFriendViewModel.MyFriendViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyFriendViewModel.MyFriendViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyFriendFragment extends Fragment implements MyFriendAdapter.Callbacks {
    public static final String TAG = MyFriendFragment.class.getSimpleName();

    private Unbinder mUnbinder;
    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;
    private MyFriendAdapter mAdapter;

    @BindView(R.id.errorProb)
    protected TextView errorProb;
    private MyFriendViewModel mViewModel;

    public static MyFriendFragment newInstance() {
        return new MyFriendFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        return view;
    }

    private void initView() {
        mAdapter = new MyFriendAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new MyFriendViewModelFactory(getActivity().getApplication())).get(MyFriendViewModel.class);
        mViewModel.getMyFriendAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyFriendAdapter.DataHolder> dataHolders) {
                if (dataHolders == null) {
                    mRecyclerView.setVisibility(View.GONE);
                    errorProb.setVisibility(View.VISIBLE);
                }
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();
            }
        });
        // Load data into adapter.
        mViewModel.loadFriendAPIcall(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    }


    @Override
    public void onListItemClicked(MyFriendAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra("friendID", data.id);
        startActivity(intent);
    }

}
