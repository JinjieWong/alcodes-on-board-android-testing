package com.demoapp.alcodesonboard.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.ImageFullScreenActivity;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyFriendDetailFragment extends Fragment {
    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    private RequestQueue mQueue;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;
    @BindView(R.id.myFriendDetailProfilePic)
    CircleImageView myFriendDetailProfilePic;
    @BindView(R.id.errorProb)
    TextView textViewErrorProb;
    @BindView(R.id.textViewFirstNameForXML)
    TextView TextViewFirstNameForXML;
    @BindView(R.id.textViewLastNameForXML)
    TextView TextViewLastNameForXML;
    @BindView(R.id.textViewEmailForXML)
    TextView TextViewEmailForXML;
    @BindView(R.id.textViewFirstName)
    TextView textViewFirstName;
    @BindView(R.id.textViewLastName)
    TextView textViewLastName;
    @BindView(R.id.textViewEmail)
    TextView textViewEmail;

    public MyFriendDetailFragment() {
    }

    public static MyFriendDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyFriendDetailFragment fragment = new MyFriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend_detail, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mQueue = Volley.newRequestQueue(getContext());
        jsonParse();//call api
        //should use the data inside the sql
        //but the reason why i call api is because
        //request is call api,show loading,show error when offline
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    private void initView() {

    }

    private void jsonParse() {
        final ProgressDialog pd = ProgressDialog.show(getContext(), null, "Loading");
        Intent intent = getActivity().getIntent();
        String id = intent.getStringExtra("friendID");
        String url = BuildConfig.BASE_API_URL + "users/" + id;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pd.dismiss();
                        try {
                            //get the json data from website
                            JSONObject data = response.getJSONObject("data");
                            String firstName = data.getString("first_name");
                            String lastName = data.getString("last_name");
                            String email = data.getString("email");
                            String profilePicURL = data.getString("avatar");
                            makeItVisible(profilePicURL);
                            textViewFirstName.setText(firstName);
                            textViewLastName.setText(lastName);
                            textViewEmail.setText(email);
                            Glide.with(getContext())//read the profile picture
                                    .load(profilePicURL)
                                    .into(myFriendDetailProfilePic);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            pd.dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                whenServerError();
                pd.dismiss();
            }
        });

        mQueue.add(request);
    }

    private void makeItVisible(String profileImageURL) {
        //make it visible because server can response
        textViewFirstName.setVisibility(View.VISIBLE);
        textViewLastName.setVisibility(View.VISIBLE);
        textViewEmail.setVisibility(View.VISIBLE);
        TextViewEmailForXML.setVisibility(View.VISIBLE);
        TextViewFirstNameForXML.setVisibility(View.VISIBLE);
        TextViewLastNameForXML.setVisibility(View.VISIBLE);
        myFriendDetailProfilePic.setVisibility(View.VISIBLE);
        myFriendDetailProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ImageFullScreenActivity.class);
                intent.putExtra("image_url", profileImageURL);
                startActivity(intent);
            }
        });
        textViewEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // when the user click email with intent to gmail
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "email@email.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "EMAIL");
                intent.putExtra(Intent.EXTRA_TEXT, "ABC");
                startActivity(Intent.createChooser(intent, "Choose an Email client :"));
            }
        });
    }

    private void whenServerError() {

        textViewErrorProb.setVisibility(View.VISIBLE);
    }
}
