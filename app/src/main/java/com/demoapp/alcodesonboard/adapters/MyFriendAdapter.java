package com.demoapp.alcodesonboard.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.ViewHolder> {
    private List<MyFriendAdapter.DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public MyFriendAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyFriendAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<MyFriendAdapter.DataHolder> data) {
        if (data == null) {
            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(MyFriendAdapter.Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {
        public String id;
        public String first_name;
        public String last_name;
        public String email;
        public String avatar;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.RelativeLayout_root)
        public RelativeLayout root;

        @BindView(R.id.textViewName)
        public TextView textViewName;
        @BindView(R.id.textViewEmail)
        public TextView textViewEmail;
        @BindView(R.id.profilePic)
        public CircleImageView profilePic;
        @BindView(R.id.button_delete)
        public MaterialButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindTo(MyFriendAdapter.DataHolder data, MyFriendAdapter.Callbacks callbacks) {
            resetViews();

            if (data != null) {
                //set the data
                textViewName.setText(data.first_name + " " + data.last_name);
                textViewEmail.setText(data.email);
                Glide.with(itemView.getContext()).load(data.avatar).into(profilePic);
                if (callbacks != null) {
                    root.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            callbacks.onListItemClicked(data);
                        }
                    });
                    profilePic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new MaterialDialog.Builder(view.getContext()).
                                    title(textViewName.getText().toString())
                                    .positiveText("Personal Profile")
                                    .negativeText("Cancel")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            Intent intent = new Intent(view.getContext(), MyFriendDetailActivity.class);
                                            intent.putExtra("friendID", data.id);
                                            view.getContext().startActivity(intent);
                                        }
                                    })
                                    .icon(profilePic.getDrawable()).show();// set the dialog have icon
                        }
                    });
                }
            }
        }

        public void resetViews() {
            root.setOnClickListener(null);
        }
    }

    public interface Callbacks {

        void onListItemClicked(MyFriendAdapter.DataHolder data);

    }
}