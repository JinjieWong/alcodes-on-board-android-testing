package com.demoapp.alcodesonboard.gsonmodels;


import java.util.ArrayList;
import java.util.List;

public class FriendModel {
    List<Data> data = new ArrayList<>();
    //public String email;
    public String friendName;
    public String profilePic;
    public String page;
    public String first_name;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
}
