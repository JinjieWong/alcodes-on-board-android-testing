package com.demoapp.alcodesonboard.gsonmodels;
import com.google.gson.annotations.SerializedName;
public class Data {
    @SerializedName("email")
    public String email;

    @SerializedName("firstname")
    public String friendName;

    @SerializedName("avatar")
    public String profilePic;
}
