package com.demoapp.alcodesonboard.activities;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.demoapp.alcodesonboard.R;

public class ImageFullScreenActivity extends AppCompatActivity {

    ImageView fullScreenImage;
    String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);
        fullScreenImage = findViewById(R.id.fullScreenImage);
        url = getIntent().getStringExtra("image_url");
        Glide.with(this).load(url)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)
                .into(fullScreenImage);
    }
}
