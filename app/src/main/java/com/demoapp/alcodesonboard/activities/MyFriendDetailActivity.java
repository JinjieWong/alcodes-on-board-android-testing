package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyFriendDetailFragment;
import com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment;

import butterknife.ButterKnife;

public class MyFriendDetailActivity extends AppCompatActivity {
    public String myFriendID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friend_detail);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyNoteDetailFragment.TAG) == null) {
            // Init fragment.
            Intent extra = getIntent();
            long friendId = 0;

            if (extra != null) {
                friendId = extra.getLongExtra(myFriendID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendDetailFragment.newInstance(friendId), MyFriendDetailFragment.TAG)
                    .commit();
        }
    }
}
