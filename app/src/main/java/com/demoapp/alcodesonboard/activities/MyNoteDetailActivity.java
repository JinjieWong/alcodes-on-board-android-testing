package com.demoapp.alcodesonboard.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.fragments.MyNoteDetailFragment;

import butterknife.ButterKnife;

public class MyNoteDetailActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_MY_NOTE_ID = "EXTRA_LONG_MY_NOTE_ID";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_note_detail);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyNoteDetailFragment.TAG) == null) {
            // Init fragment.
            Intent extra = getIntent();
            long noteId = 0;

            if (extra != null) {
                noteId = extra.getLongExtra(EXTRA_LONG_MY_NOTE_ID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyNoteDetailFragment.newInstance(noteId), MyNoteDetailFragment.TAG)
                    .commit();
        }
    }
}
