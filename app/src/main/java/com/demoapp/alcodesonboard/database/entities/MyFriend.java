package com.demoapp.alcodesonboard.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class MyFriend {
    @NotNull
    private String first_name;

    @NotNull
    private String last_name;

    @NotNull
    private String email;
    @NotNull
    @Id(autoincrement = true)
    private Long id;
    @NotNull
    private String avatar;

    @Generated(hash = 1392558361)
    public MyFriend(@NotNull String first_name, @NotNull String last_name, @NotNull String email,
                    @NotNull Long id, @NotNull String avatar) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.id = id;
        this.avatar = avatar;
    }

    @Generated(hash = 15986203)
    public MyFriend() {
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
