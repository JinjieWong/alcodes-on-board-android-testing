package com.demoapp.alcodesonboard.viewmodels.MyFriendViewModel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.repositories.MyFriendRepository;

import java.util.List;

public class MyFriendViewModel extends AndroidViewModel {

    private MyFriendRepository myFriendRepository;

    public MyFriendViewModel(@NonNull Application application) {
        super(application);

        myFriendRepository = MyFriendRepository.getInstance();
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return myFriendRepository.getMyFriendAdapterListLiveData();
    }

    public void loadFriendAPIcall(Context context) {
        myFriendRepository.friendAPICall(context);
    }
}
