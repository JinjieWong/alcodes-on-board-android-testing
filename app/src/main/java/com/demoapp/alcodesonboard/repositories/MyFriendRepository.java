package com.demoapp.alcodesonboard.repositories;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.NetworkHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyFriendRepository {

    private static MyFriendRepository mInstance;

    private MutableLiveData<List<MyFriendAdapter.DataHolder>> mMyFriendAdapterListLiveData = new MutableLiveData<>();

    public static MyFriendRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendRepository.class) {
                mInstance = new MyFriendRepository();
            }
        }

        return mInstance;
    }

    public MyFriendRepository() {
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return mMyFriendAdapterListLiveData;
    }

    public void loadMyFriendAdapterList(Context context) {
        List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if (records != null) {
            for (MyFriend myFriend : records) {
                MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
                dataHolder.id = myFriend.getId().toString();
                dataHolder.first_name = myFriend.getFirst_name();
                dataHolder.last_name = myFriend.getLast_name();
                dataHolder.email = myFriend.getEmail();
                dataHolder.avatar = myFriend.getAvatar();
                dataHolders.add(dataHolder);
            }
        }

        mMyFriendAdapterListLiveData.setValue(dataHolders);
    }

    public void friendAPICall(Context context) {
        final ProgressDialog pd = ProgressDialog.show(context, null, "Loading");
        // call the api and save the json into database
        String url = BuildConfig.BASE_API_URL + "users?page=1";
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    pd.dismiss();
                    JSONArray Jarry = response.getJSONArray("data");
                    for (int i = 0; i < Jarry.length(); i++) {
                        MyFriend friend = new MyFriend();
                        JSONObject data = Jarry.getJSONObject(i);
                        friend.setId(data.getLong("id"));
                        friend.setFirst_name(data.getString("first_name"));
                        friend.setLast_name(data.getString("last_name"));
                        friend.setEmail(data.getString("email"));
                        friend.setAvatar(data.getString("avatar"));
                        // Add record to database.
                        DatabaseHelper.getInstance(context)
                                .getMyFriendDao()
                                .insertOrReplace(friend);

                        // Done adding record, now re-load list.
                        loadMyFriendAdapterList(context);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                new MaterialDialog.Builder(context)
                        .title("Error")
                        .content("Please access your network.")
                        .positiveText("OK")
                        .show();
                error.printStackTrace();
            }
        });

        NetworkHelper.getRequestQueueInstance(context).add(stringRequest);
    }
}
